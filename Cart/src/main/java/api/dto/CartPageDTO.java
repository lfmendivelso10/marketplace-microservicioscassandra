package api.dto;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

/**
 * Created by Mauricio on 3/23/15.
 */
@XmlRootElement
public class CartPageDTO {
    private Long totalRecords;

    private List<CartDTO> records;

    public Long getTotalRecords() {
        return totalRecords;
    }

    public void setTotalRecords(Long totalRecords) {
        this.totalRecords = totalRecords;
    }

    public List<CartDTO> getRecords() {
        return records;
    }

    public void setRecords(List<CartDTO> records) {
        this.records = records;
    }
}
