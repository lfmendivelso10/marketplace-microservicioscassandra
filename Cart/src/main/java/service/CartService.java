package service;

import api.api.ICartLogicService;
import api.dto.CartDTO;
import api.dto.CartPageDTO;
import logic.CartLogicService;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

/**
 * Created by Mauricio on 3/23/15.
 */
@Path("/Cart")
@Stateless
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class CartService {
    @Inject
    protected CartLogicService cartLogicService;

    @POST
    public CartDTO createCart(CartDTO cart){
        return cartLogicService.createCart(cart);
    }

    @DELETE
    @Path("{id}")
    public void deleteCart(@PathParam("id") Long id){
        cartLogicService.deleteCart(id);
    }

    @GET
    public CartPageDTO getCarts(@QueryParam("page") Integer page, @QueryParam("maxRecords") Integer maxRecords){
        return cartLogicService.getCarts(page, maxRecords);
    }

    @GET
    @Path("{id}")
    public CartDTO getCart(@PathParam("id") Long id){
        return cartLogicService.getCart(id);
    }

    @PUT
    public void updateCart(@PathParam("id") Long id, CartDTO cart){
        cartLogicService.updateCart(cart);
    }

}
