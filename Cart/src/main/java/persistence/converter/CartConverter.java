package persistence.converter;

import api.dto.CartDTO;
import persistence.entity.CartEntity;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Mauricio on 3/23/15.
 */
public class CartConverter {
    public static CartDTO entity2PersistenceDTO(CartEntity entity){
        if (entity != null) {
            CartDTO dto = new CartDTO();
            dto.setId(entity.getId());
            return dto;
        }else{
            return null;
        }
    }

    public static CartEntity persistenceDTO2Entity(CartDTO dto){
        if(dto!=null){
            CartEntity entity=new CartEntity();
            entity.setId(dto.getId());

            return entity;
        }else {
            return null;
        }
    }

    public static List<CartDTO> entity2PersistenceDTOList(List<CartEntity> entities){
        List<CartDTO> dtos=new ArrayList<CartDTO>();
        for(CartEntity entity:entities){
            dtos.add(entity2PersistenceDTO(entity));
        }
        return dtos;
    }

    public static List<CartEntity> persistenceDTO2EntityList(List<CartDTO> dtos){
        List<CartEntity> entities=new ArrayList<CartEntity>();
        for(CartDTO dto:dtos){
            entities.add(persistenceDTO2Entity(dto));
        }
        return entities;
    }
}
