package persistence.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * Created by Mauricio on 3/23/15.
 */
@Entity
public class CartEntity {

    @Id
    @GeneratedValue(generator = "Cart")
    private Long id;

    public Long getId(){
        return id;
    }

    public void setId(Long id){
        this.id = id;
    }
}
