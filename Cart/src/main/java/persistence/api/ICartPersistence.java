package persistence.api;

import api.dto.CartDTO;
import api.dto.CartPageDTO;

import java.util.List;

/**
 * Created by Mauricio on 3/23/15.
 */
public interface ICartPersistence {

    public CartDTO createCart(CartDTO detail);
    public List<CartDTO> getCarts();
    public CartPageDTO getCarts(Integer page, Integer maxRecords);
    public CartDTO getCart(Long id);
    public void deleteCart(Long id);
    public void updateCart(CartDTO detail);
}
