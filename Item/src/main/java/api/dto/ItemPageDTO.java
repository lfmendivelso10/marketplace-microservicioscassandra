package api.dto;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

/**
 * Created by Mauricio on 3/23/15.
 */
@XmlRootElement
public class ItemPageDTO {

    private Long totalRecords;

    private List<ItemDTO> records;

    public Long getTotalRecords() {
        return totalRecords;
    }

    public void setTotalRecords(Long totalRecords) {
        this.totalRecords = totalRecords;
    }

    public List<ItemDTO> getRecords() {
        return records;
    }

    public void setRecords(List<ItemDTO> records) {
        this.records = records;
    }
}
