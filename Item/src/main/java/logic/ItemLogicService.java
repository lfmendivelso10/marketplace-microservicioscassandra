package logic;

import api.api.IItemLogicService;
import api.dto.ItemDTO;
import api.dto.ItemPageDTO;
import persistence.ItemPersistence;
import persistence.api.IItemPersistence;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.List;

/**
 * Created by Mauricio on 3/23/15.
 */
//@Default
@Stateless
@LocalBean
public class ItemLogicService implements IItemLogicService{

    @Inject
    private ItemPersistence persistance;

    public ItemDTO createItem(ItemDTO item){
        return persistance.createItem( item);
    }

    public List<ItemDTO> getItems(){
        return persistance.getItems();
    }

    public ItemPageDTO getItems(Integer page, Integer maxRecords){
        return persistance.getItems(page, maxRecords);
    }

    public ItemDTO getItem(Long id){
        return persistance.getItem(id);
    }

    public void deleteItem(Long id){
        persistance.deleteItem(id);
    }

    public void updateItem(ItemDTO item){
        persistance.updateItem(item);
    }
}
