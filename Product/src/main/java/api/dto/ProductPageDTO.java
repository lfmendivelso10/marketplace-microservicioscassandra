package api.dto;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

/**
 * Created by Mauricio on 3/23/15.
 */
@XmlRootElement
public class ProductPageDTO {
    private Long totalRecords;

    private List<ProductDTO> records;

    public Long getTotalRecords() {
        return totalRecords;
    }

    public void setTotalRecords(Long totalRecords) {
        this.totalRecords = totalRecords;
    }

    public List<ProductDTO> getRecords() {
        return records;
    }

    public void setRecords(List<ProductDTO> records) {
        this.records = records;
    }
}
