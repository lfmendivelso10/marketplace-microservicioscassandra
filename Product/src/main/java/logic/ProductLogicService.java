package logic;

import api.api.IProductLogicService;
import api.dto.ProductDTO;
import api.dto.ProductPageDTO;
import persistence.ProductPersistence;
import persistence.api.IProductPersistence;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.enterprise.inject.Default;
import javax.inject.Inject;
import java.util.List;

/**
 * Created by Mauricio on 3/23/15.
 */
//@Default
@Stateless
@LocalBean
public class ProductLogicService implements IProductLogicService{

    @Inject
    private ProductPersistence persistance;

    public ProductDTO createProduct(ProductDTO product){
        return persistance.createProduct( product);
    }

    public List<ProductDTO> getProducts(){
        return persistance.getProducts();
    }

    public ProductPageDTO getProducts(Integer page, Integer maxRecords){
        return persistance.getProducts(page, maxRecords);
    }

    public ProductDTO getProduct(Long id){
        return persistance.getProduct(id);
    }

    public void deleteProduct(Long id){
        persistance.deleteProduct(id);
    }

    public void updateProduct(ProductDTO product){
        persistance.updateProduct(product);
    }

    public String hola(){
        return persistance.hola();
    }
}
