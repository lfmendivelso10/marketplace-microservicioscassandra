package service;

import api.api.IProductLogicService;
import api.dto.ProductDTO;
import api.dto.ProductPageDTO;
import logic.ProductLogicService;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;

/**
 * Created by Mauricio on 3/23/15.
 */
@Path("/Product")
@Stateless
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class ProductService {
    @Inject
    protected ProductLogicService productLogicService;

    @POST
    public ProductDTO createProduct(ProductDTO product){
        return productLogicService.createProduct(product);
    }

    @DELETE
    @Path("{id}")
    public void deleteProduct(@PathParam("id") Long id){
        productLogicService.deleteProduct(id);
    }

    @GET
    public ProductPageDTO getProducts(@QueryParam("page") Integer page, @QueryParam("maxRecords") Integer maxRecords){
        return productLogicService.getProducts(page, maxRecords);
    }

    @GET
    @Path("{id}")
    public ProductDTO getProduct(@PathParam("id") Long id){
        return productLogicService.getProduct(id);
    }

    @PUT
    public void updateProduct(@PathParam("id") Long id, ProductDTO product){
        productLogicService.updateProduct(product);
    }

    @GET
    @Path("/hola")
    public String get(){
//        return productLogicService.hola();
        Client client = ClientBuilder.newClient();
        WebTarget target = client.target("http://localhost:8080").path("Product/webresources/Product/");
        ProductPageDTO f=target.request(MediaType.APPLICATION_JSON).get(ProductPageDTO.class);
        return f.getRecords().toString();
    }
}
