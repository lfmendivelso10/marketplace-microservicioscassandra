package service;


import api.dto.ProductPageDTO;
import example.ProductLogicService;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;

/**
 * Created by Mauricio on 3/22/15.
 */
@Path("/test")
@Produces(MediaType.APPLICATION_JSON)
public class Example {

    @Inject
    private ProductLogicService product;

    @GET
    public ProductPageDTO f(){

        Client client = ClientBuilder.newClient();
        WebTarget target = client.target("http://localhost:8080").path("/Example/webresources/Product/");

        return target.request(MediaType.APPLICATION_JSON).get(ProductPageDTO.class);

//        return "se odio: "+product.getProducts().toString();
    }

    @GET
    @Path("el")
    public String dsf(){
        return "entro";
    }
}
