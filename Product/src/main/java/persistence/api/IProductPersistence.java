package persistence.api;

import api.dto.ProductDTO;
import api.dto.ProductPageDTO;

import java.util.List;

/**
 * Created by Mauricio on 3/23/15.
 */
public interface IProductPersistence {
    public ProductDTO createProduct(ProductDTO detail);
    public List<ProductDTO> getProducts();
    public ProductPageDTO getProducts(Integer page, Integer maxRecords);
    public ProductDTO getProduct(Long id);
    public void deleteProduct(Long id);
    public void updateProduct(ProductDTO detail);
}
