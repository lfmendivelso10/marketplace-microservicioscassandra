# Proyecto - Marketplace #

El presente proyecto corresponde a un Marketplace con una estilo de arquitectura de Microservicios, el cual será como base para evaluar la latencia, escalabilidad, disponibilidad y consistencia eventual lograda mediante el esquema Google Megastore.

## Entorno:
JDK 1.7 o más
Cassandra 2.11

## Calidad de Código:
[![Codacy Badge](https://api.codacy.com/project/badge/grade/9c4259bb4d63436a8bd803dea0727546)](https://www.codacy.com/app/ingfelipemendivelso/marketplace-microservicioscassandra)


## Integración